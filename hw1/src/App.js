import './App.css';
import TogglerFunction from './TogglerFunction';
import TogglerClass from './TogglerClass';

function App() {
  
  return (
    <div className="App">
      <TogglerClass off={<div className="light_off"/>} on={<div className="light_on"/>} />
      <TogglerFunction />
    </div>
  );
}

export default App;
