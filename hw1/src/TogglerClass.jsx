import React, { usestate } from 'react';


class Toggler extends React.Component {

  state = {
    isToggler: false,
  }

  toggle = () => {
    this.setState({
      isToggler: !this.state.isToggler,
    })
  }

  render(){
    return(
      <div className="container_light">
        {this.state.isToggler ? this.props.off : this.props.on}
        <span>
          <a href="#" onClick={this.toggle}>
          </a>
        </span> 
      </div>
    )
  }
}

export default Toggler