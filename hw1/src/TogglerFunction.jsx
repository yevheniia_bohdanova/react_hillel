import React from 'react'
import { useState } from 'react'

export default function TogglerFunction() {
  const [day, setDay] = useState(true);
  let time = '';
  let remind = '';
  
  const funcForTurn = () => {
    setDay(!day)
  }

  const renderTheme = () => {
    if (day) {
      time = "Day";
      remind = `Don't forget to turn off the light`;
      return document.documentElement.setAttribute('data-theme', 'light');
    } else {
      time = "Night";
      return document.documentElement.setAttribute('data-theme', 'dark')}
  }

  return (
    <div>
      { renderTheme() }
      <h2 className="remind">{remind}</h2>
      <div>
        <h1>{time}</h1>
        <input onClick={funcForTurn} className="container_toggle" type="checkbox" id="switch" name="mode"/>
        <label for="switch">Toggle</label>
      </div>
    </div>
  )
}

